import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import CreatePost from "./CreatePost";
import ListingPost from "./ListingPost";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<ListingPost />} />
        <Route path="/create-post" element={<CreatePost />} />
      </Routes>
    </Router>
  );
}

export default App;
