import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Box,
  Button,
  ChakraProvider,
  Container,
  Divider,
  Heading,
  Text,
  Wrap,
  WrapItem,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";

function ListingPost() {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then((response) => {
        setPosts(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <ChakraProvider>
      <Container maxW={"7xl"} p="12">
        <Heading as="h2" marginTop="5" marginBottom={"5"}>
          <h1>List of Posts</h1>
        </Heading>
        <Link to={"/create-post"}>
          <Button
            px={8}
            color={"skyblue"}
            rounded={"md"}
            _hover={{
              transform: "translateY(-2px)",
              boxShadow: "lg",
            }}
          >
            Create Post
          </Button>
        </Link>
        <Divider marginTop="5" />
        <ul>
          {posts.map((post) => (
            <Wrap spacing="30px" marginTop="5">
              <WrapItem
                width={{ base: "100%", sm: "100%", md: "100%", lg: "100%" }}
              >
                <Box w="100%">
                  <li key={post.id}>
                    <Heading fontSize="xl" marginTop="2">
                      <h2>{post.title}</h2>
                    </Heading>
                    <Text as="p" fontSize="md" marginTop="2">
                      <p>{post.body}</p>
                    </Text>
                  </li>
                  <Divider marginTop="5" />
                </Box>
              </WrapItem>
            </Wrap>
          ))}
        </ul>
      </Container>
    </ChakraProvider>
  );
}

export default ListingPost;
