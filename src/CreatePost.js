import axios from "axios";
import { useState } from "react";
import {
  FormControl,
  FormLabel,
  Input,
  Button,
  VStack,
  Box,
  ChakraProvider,
  Container,
  Heading,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { AiFillHome } from "react-icons/ai";

function CreatePost() {
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    const newPost = {
      title: title,
      body: body,
    };
    axios
      .post("https://jsonplaceholder.typicode.com/posts", newPost)
      .then((response) => {
        console.log(response.data);
      });
  };

  return (
    <ChakraProvider>
      <Container marginTop={"10"}>
        <Box>
          <Heading marginBottom={"5"}>FORM CREATE POST</Heading>
          <form onSubmit={handleSubmit}>
            <VStack spacing="4">
              <FormControl id="title">
                <FormLabel>Judul</FormLabel>
                <Input
                  type="text"
                  value={title}
                  onChange={(event) => setTitle(event.target.value)}
                />
              </FormControl>
              <FormControl id="body">
                <FormLabel>Isi</FormLabel>
                <Input
                  type="text"
                  value={body}
                  onChange={(event) => setBody(event.target.value)}
                />
              </FormControl>
              <Button type="submit">Buat Post Baru</Button>
              <Link to={"/"}>
                <Button>
                  <AiFillHome />
                </Button>
              </Link>
            </VStack>
          </form>
        </Box>
      </Container>
    </ChakraProvider>
  );
}

export default CreatePost;
